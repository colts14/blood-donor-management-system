﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonorClient.Models
{
    public class Donor
    {
        public int id { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string gender { get; set; }
        public DateTime dateOfBirth { get; set; }
        public int age { get; set; }
        public string maritalStatus { get; set; }
        public string occupation { get; set; }
        public string bloodGroup { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string postZipCode { get; set; }
        public string city { get; set; }
        public string provState { get; set; }
        public string country { get; set; }
        public string phone { get; set; }
        public string mobile { get; set; }
        public DateTime timestamp { get; set; }
    }
}
