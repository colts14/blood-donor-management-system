﻿using DonorClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Windows.Forms;


namespace DonorClient
{
    public partial class Form1 : Form
    {
        //---- Private fields ----
        private int donorId;
        private ColumnSorter m_lstColumnSorter;
        private static RestClient restClient;
        private static String baseUrl;
        private const String APIKEY = "tg4zhuThPFClGyBSlgpyrIUYvl3UgJ86";

        public static string BaseUrl { get => baseUrl; set => baseUrl = value; }
        public static RestClient RestClient { get => restClient; set => restClient = value; }

        //---- Constructor ----
        public Form1()
        {
            InitializeComponent();
            BuildListViewSpace();
            BaseUrl = "http://rpolintan17-eval-test.apigee.net/donor-management-api/api/donors";
            //BaseUrl = "http://active-cove-179316.appspot.com/api/donors";
            RestClient = new RestClient(APIKEY);

        }


        //---- Build the listview ----
        private void BuildListViewSpace()
        {
            // Set the view to show details.
            listView1.View = View.Details;
            // Select the item and subitems when selection is made.
            listView1.FullRowSelect = true;
            // Display grid lines.
            listView1.GridLines = true;
            // Create columns with specific width sizes
            listView1.Columns.Add("Id", -2, HorizontalAlignment.Left);
            listView1.Columns.Add("Blood Group", -2, HorizontalAlignment.Left);
            listView1.Columns.Add("First Name", -2, HorizontalAlignment.Left);
            listView1.Columns.Add("Last Name", -2, HorizontalAlignment.Left);
            listView1.Columns.Add("Gender", -2, HorizontalAlignment.Left);
            listView1.Columns.Add("Date of Birth", -2, HorizontalAlignment.Left);
            listView1.Columns.Add("Age", -2, HorizontalAlignment.Left);
            listView1.Columns.Add("Address 1", -2, HorizontalAlignment.Left);
            listView1.Columns.Add("Address 2", -2, HorizontalAlignment.Left);
            listView1.Columns.Add("City", -2, HorizontalAlignment.Left);
            listView1.Columns.Add("Province / State", -2, HorizontalAlignment.Left);
            listView1.Columns.Add("Postal / Zip Code", -2, HorizontalAlignment.Left);
            listView1.Columns.Add("Country", -2, HorizontalAlignment.Left);
            listView1.Columns.Add("Phone", -2, HorizontalAlignment.Left);
            listView1.Columns.Add("Mobile", -2, HorizontalAlignment.Left);
        }

        private async void ListDonors(object sender, EventArgs e)
        {
            ResetValues();

            List<Donor> donors = await RestClient.GetRequestAsync(BaseUrl);
            listView1.Items.Clear();

            if (donors != null)
            {
                foreach (Donor donor in donors)
                {
                    AddItem(donor);
                }
            }

            m_lstColumnSorter = new ColumnSorter();
            //Specify the listviewcolumnsorter 
            listView1.ListViewItemSorter = m_lstColumnSorter;

        }

        private void listView1_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            ListView myListView = (ListView)sender;

            if (m_lstColumnSorter != null)
            {
                // Determine if clicked column is already the column that is being sorted.
                if (e.Column == m_lstColumnSorter.SortColumn)
                {
                    // Reverse the current sort direction for this column.
                    if (m_lstColumnSorter.Order == SortOrder.Ascending)
                    {
                        m_lstColumnSorter.Order = SortOrder.Descending;
                    }
                    else
                    {
                        m_lstColumnSorter.Order = SortOrder.Ascending;
                    }
                }
                else
                {
                    // Set the column number that is to be sorted; default to ascending.
                    m_lstColumnSorter.SortColumn = e.Column;
                    m_lstColumnSorter.Order = SortOrder.Ascending;
                }
                // Perform the sort with these new sort options.
                myListView.Sort();
                myListView.SetSortIcon(m_lstColumnSorter.SortColumn, m_lstColumnSorter.Order);
            }
        }

        private async void btnListCompatible_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();
            if (tbRecipient.Text.Trim() == "")
            {
                tbMessages.Text = "Please enter recipient blood group.";
                return;
            }

            ResetValues();
            List<Donor> donors = await RestClient.GetRequestAsync(BaseUrl);

            foreach (Donor donor in donors)
            {
                String bloodGroup = donor.bloodGroup.Trim().ToUpper();
                switch (tbRecipient.Text.Trim().ToUpper())
                {
                    case "O-":
                        if (bloodGroup.Equals("O-"))
                        {
                            AddItem(donor);
                        }
                        break;

                    case "O+":
                        if ((bloodGroup.Equals("O-")) || (bloodGroup.Equals("O+")))
                        {
                            AddItem(donor);
                        }
                        break;

                    case "A-":
                        if ((bloodGroup.Equals("O-")) || (bloodGroup.Equals("A-")))
                        {
                            AddItem(donor);
                        }
                        break;

                    case "A+":
                        if ((bloodGroup.Equals("O-")) || (bloodGroup.Equals("O+")) ||
                            (bloodGroup.Equals("A-")) || (bloodGroup.Equals("A+")))
                        {
                            AddItem(donor);
                        }
                        break;

                    case "B-":
                        if ((bloodGroup.Equals("O-")) || (bloodGroup.Equals("B-")))
                        {
                            AddItem(donor);
                        }
                        break;

                    case "B+":
                        if ((bloodGroup.Equals("O-")) || (bloodGroup.Equals("O+")) ||
                            (bloodGroup.Equals("B-")) || (bloodGroup.Equals("B+")))
                        {
                            AddItem(donor);
                        }
                        break;

                    case "AB-":
                        if ((bloodGroup.Equals("O-")) || (bloodGroup.Equals("A-")) ||
                            (bloodGroup.Equals("B-")) || (bloodGroup.Equals("AB-")))
                        {
                            AddItem(donor);
                        }
                        break;

                    case "AB+":
                        if ((bloodGroup.Equals("O-")) || (bloodGroup.Equals("O+")) ||
                            (bloodGroup.Equals("A-")) || (bloodGroup.Equals("A+")) ||
                            (bloodGroup.Equals("B-")) || (bloodGroup.Equals("B+")) ||
                            (bloodGroup.Equals("AB-")) || (bloodGroup.Equals("AB+")))
                        {
                            AddItem(donor);
                        }
                        break;
                }
            }

            m_lstColumnSorter = new ColumnSorter();
            //Specify the listviewcolumnsorter 
            listView1.ListViewItemSorter = m_lstColumnSorter;
        }

        private void AddItem(Donor donor)
        {
            ListViewItem lvItem = new ListViewItem(donor.id.ToString());
            lvItem.SubItems[listView1.Columns.OfType<ColumnHeader>().First(col => col.Text == "Id").Index].Tag = Convert.ToInt32(donor.id);
            lvItem.SubItems.Add(donor.bloodGroup);
            lvItem.SubItems.Add(donor.firstName);
            lvItem.SubItems.Add(donor.lastName);
            lvItem.SubItems.Add(donor.gender);
            lvItem.SubItems.Add(donor.dateOfBirth.ToShortDateString());
            lvItem.SubItems[listView1.Columns.OfType<ColumnHeader>().First(col => col.Text == "Date of Birth").Index].Tag = donor.dateOfBirth;
            lvItem.SubItems.Add(donor.age.ToString());
            lvItem.SubItems[listView1.Columns.OfType<ColumnHeader>().First(col => col.Text == "Age").Index].Tag = Convert.ToInt32(donor.age);
            lvItem.SubItems.Add(donor.address1);
            lvItem.SubItems.Add(donor.address2);
            lvItem.SubItems.Add(donor.city);
            lvItem.SubItems.Add(donor.provState);
            lvItem.SubItems.Add(donor.postZipCode);
            lvItem.SubItems.Add(donor.country);
            lvItem.SubItems.Add(donor.phone);
            lvItem.SubItems.Add(donor.mobile);
            listView1.Items.Add(lvItem);
        }

        private async void btnDelete_Click(object sender, EventArgs e)
        {
            if (!(donorId > 0))
            {
                tbMessages.Text = "Please select a donor first.\r\nBefore clicking on \"Delete Donor.\"";
                return;
            }

            DialogResult result = MessageBox.Show(this, "Please confirm you want to delete donor with Id: " + donorId,
                "Confirm Deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);

            if (result == DialogResult.Yes)
            {
                HttpResponseMessage response = await RestClient.DeleteRequestAsync(BaseUrl + "/" + donorId);
                tbMessages.Text = "Status code: " + response.StatusCode.ToString();
                ResetValues();
                ListDonors(sender, e);
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)

            {
                donorId = Convert.ToInt32(listView1.SelectedItems[0].Text);
            }
        }

        private void btnAddDonor_Click(object sender, EventArgs e)
        {
            Form3 frm3 = new Form3();
            frm3.Show();
            ResetValues();
            this.Hide();
            frm3.FormClosing += Frm_Closing;
        }

        private void btnUpdateDonor_Click(object sender, EventArgs e)
        {
            if (!(donorId > 0))
            {
                tbMessages.Text = "Please select a donor first.\r\nBefore clicking on \"Update Donor.\"";
                return;
            }
            Form2 frm2 = new Form2(donorId);
            frm2.Show();
            ResetValues();
            this.Hide();
            frm2.FormClosing += Frm_Closing;
        }

        private void Frm_Closing(object sender, FormClosingEventArgs e)
        {
            this.Show();
            ListDonors(sender, e);
        }

        private void ResetValues()
        {
            donorId = 0;
            tbMessages.Text = "";
        }
    }
}

