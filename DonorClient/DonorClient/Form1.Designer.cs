﻿namespace DonorClient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView1 = new System.Windows.Forms.ListView();
            this.grpbMessages = new System.Windows.Forms.GroupBox();
            this.tbMessages = new System.Windows.Forms.TextBox();
            this.btnListAllDonors = new System.Windows.Forms.Button();
            this.btnListCompatible = new System.Windows.Forms.Button();
            this.tbRecipient = new System.Windows.Forms.TextBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnUpdateDonor = new System.Windows.Forms.Button();
            this.btnAddDonor = new System.Windows.Forms.Button();
            this.lblRecipient = new System.Windows.Forms.Label();
            this.grpbMessages.SuspendLayout();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listView1.Location = new System.Drawing.Point(12, 24);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(1060, 285);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.listView1_ColumnClick);
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // grpbMessages
            // 
            this.grpbMessages.Controls.Add(this.tbMessages);
            this.grpbMessages.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpbMessages.Location = new System.Drawing.Point(618, 324);
            this.grpbMessages.Name = "grpbMessages";
            this.grpbMessages.Size = new System.Drawing.Size(357, 95);
            this.grpbMessages.TabIndex = 8;
            this.grpbMessages.TabStop = false;
            this.grpbMessages.Text = "Messages";
            // 
            // tbMessages
            // 
            this.tbMessages.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMessages.Location = new System.Drawing.Point(7, 23);
            this.tbMessages.Multiline = true;
            this.tbMessages.Name = "tbMessages";
            this.tbMessages.ReadOnly = true;
            this.tbMessages.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbMessages.Size = new System.Drawing.Size(344, 62);
            this.tbMessages.TabIndex = 0;
            // 
            // btnListAllDonors
            // 
            this.btnListAllDonors.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListAllDonors.Location = new System.Drawing.Point(83, 391);
            this.btnListAllDonors.Name = "btnListAllDonors";
            this.btnListAllDonors.Size = new System.Drawing.Size(150, 32);
            this.btnListAllDonors.TabIndex = 4;
            this.btnListAllDonors.Text = "List All Donors";
            this.btnListAllDonors.UseVisualStyleBackColor = true;
            this.btnListAllDonors.Click += new System.EventHandler(this.ListDonors);
            // 
            // btnListCompatible
            // 
            this.btnListCompatible.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListCompatible.Location = new System.Drawing.Point(83, 353);
            this.btnListCompatible.Name = "btnListCompatible";
            this.btnListCompatible.Size = new System.Drawing.Size(150, 32);
            this.btnListCompatible.TabIndex = 3;
            this.btnListCompatible.Text = "List Compatible";
            this.btnListCompatible.UseVisualStyleBackColor = true;
            this.btnListCompatible.Click += new System.EventHandler(this.btnListCompatible_Click);
            // 
            // tbRecipient
            // 
            this.tbRecipient.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbRecipient.Location = new System.Drawing.Point(183, 322);
            this.tbRecipient.Name = "tbRecipient";
            this.tbRecipient.Size = new System.Drawing.Size(50, 23);
            this.tbRecipient.TabIndex = 2;
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Location = new System.Drawing.Point(309, 398);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(150, 32);
            this.btnDelete.TabIndex = 7;
            this.btnDelete.Text = "Delete Donor";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnUpdateDonor
            // 
            this.btnUpdateDonor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdateDonor.Location = new System.Drawing.Point(309, 360);
            this.btnUpdateDonor.Name = "btnUpdateDonor";
            this.btnUpdateDonor.Size = new System.Drawing.Size(150, 32);
            this.btnUpdateDonor.TabIndex = 6;
            this.btnUpdateDonor.Text = "Update Donor";
            this.btnUpdateDonor.UseVisualStyleBackColor = true;
            this.btnUpdateDonor.Click += new System.EventHandler(this.btnUpdateDonor_Click);
            // 
            // btnAddDonor
            // 
            this.btnAddDonor.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddDonor.Location = new System.Drawing.Point(309, 322);
            this.btnAddDonor.Name = "btnAddDonor";
            this.btnAddDonor.Size = new System.Drawing.Size(150, 32);
            this.btnAddDonor.TabIndex = 5;
            this.btnAddDonor.Text = "Add Donor";
            this.btnAddDonor.UseVisualStyleBackColor = true;
            this.btnAddDonor.Click += new System.EventHandler(this.btnAddDonor_Click);
            // 
            // lblRecipient
            // 
            this.lblRecipient.AutoSize = true;
            this.lblRecipient.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRecipient.Location = new System.Drawing.Point(26, 325);
            this.lblRecipient.Name = "lblRecipient";
            this.lblRecipient.Size = new System.Drawing.Size(155, 17);
            this.lblRecipient.TabIndex = 1;
            this.lblRecipient.Text = "Recipient Blood Group:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1084, 441);
            this.Controls.Add(this.lblRecipient);
            this.Controls.Add(this.btnAddDonor);
            this.Controls.Add(this.btnUpdateDonor);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.tbRecipient);
            this.Controls.Add(this.btnListCompatible);
            this.Controls.Add(this.btnListAllDonors);
            this.Controls.Add(this.grpbMessages);
            this.Controls.Add(this.listView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Donor Management";
            this.grpbMessages.ResumeLayout(false);
            this.grpbMessages.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.GroupBox grpbMessages;
        private System.Windows.Forms.TextBox tbMessages;
        private System.Windows.Forms.Button btnListAllDonors;
        private System.Windows.Forms.Button btnListCompatible;
        private System.Windows.Forms.TextBox tbRecipient;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnUpdateDonor;
        private System.Windows.Forms.Button btnAddDonor;
        private System.Windows.Forms.Label lblRecipient;
    }
}

