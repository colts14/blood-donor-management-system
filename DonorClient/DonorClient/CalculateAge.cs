﻿//---- General age calculation adapted from https://stackoverflow.com/questions/9/how-do-i-calculate-someones-age-in-c ----//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DonorClient
{
    public static class CalculateAge
    {
        public static int GetAge(DateTime birth, DateTime today)
        {
            today = DateTime.Today;
            int age = today.Year - birth.Year;    //people perceive their age in years

            if (today.Month < birth.Month ||
               ((today.Month == birth.Month) && (today.Day < birth.Day)))
            {
                age--;  //birthday in current year not yet reached, we are 1 year younger ;)
                        //+ no birthday for 29.2. guys ... sorry, just wrong date for birth
            }
            return age;
        }

        public static int GetAge(DateTime birth)
        {
            return GetAge(birth, DateTime.Now);
        }
    }
}
