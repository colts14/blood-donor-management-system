﻿using DonorClient.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DonorClient
{
    public class RestClient
    {
        private String apiKey;

        public RestClient(String apiKey)
        {
            this.apiKey = apiKey;
        }

        public async Task<List<Donor>> GetRequestAsync(String apiUrl)
        {
            using (var client = new HttpClient())
            {
                String restUrl = apiUrl + "?apikey=" + apiKey;
                //String restUrl = apiUrl;
                HttpResponseMessage response = await client.GetAsync(restUrl);
                if (response.IsSuccessStatusCode)
                {
                    String result = await response.Content.ReadAsStringAsync();
                    List<Donor> donorList = JsonConvert.DeserializeObject<List<Donor>>(result);
                    return donorList;
                }
                else
                {
                    return null;
                }
            }
        }

        public async Task<Donor> GetSingleRequestAsync(String apiUrl)
        {
            using (var client = new HttpClient())
            {
                String restUrl = apiUrl + "?apikey=" + apiKey;
                //String restUrl = apiUrl;
                HttpResponseMessage response = await client.GetAsync(restUrl);
                if (response.IsSuccessStatusCode)
                {
                    String result = await response.Content.ReadAsStringAsync();
                    Donor donor = JsonConvert.DeserializeObject<Donor>(result);
                    return donor;
                }
                else
                {
                    return null;
                }
            }
        }

        public async Task<HttpResponseMessage> PutRequestAsync(String apiUrl, Donor donor)
        {
            String jsonOutput = JsonConvert.SerializeObject(donor, Formatting.Indented);
            StringContent stringContent = new StringContent(jsonOutput, UnicodeEncoding.UTF8, "application/json");

            using (var client = new HttpClient())
            {
                String restUrl = apiUrl + "?apikey=" + apiKey;
                //String restUrl = apiUrl;
                return await client.PutAsync(restUrl, stringContent);
            }
        }

        public async Task<HttpResponseMessage> PostRequestAsync(String apiUrl, Donor donor)
        {
            String jsonOutput = JsonConvert.SerializeObject(donor, Formatting.Indented);
            StringContent stringContent = new StringContent(jsonOutput, UnicodeEncoding.UTF8, "application/json");

            using (var client = new HttpClient())
            {
                String restUrl = apiUrl + "?apikey=" + apiKey;
                //String restUrl = apiUrl;
                return await client.PostAsync(restUrl, stringContent);
            }
        }
        
        public async Task<HttpResponseMessage> DeleteRequestAsync(String apiUrl)
        {
            using (var client = new HttpClient())
            {
                String restUrl = apiUrl + "?apikey=" + apiKey;
                //String restUrl = apiUrl;
                return await client.DeleteAsync(restUrl);
            }
        }
    }
}
