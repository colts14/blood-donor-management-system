﻿using DonorClient.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DonorClient
{
    public partial class Form3 : Form
    {
        private static RestClient restClient = Form1.RestClient;
        private static String baseUrl = Form1.BaseUrl;

        public Form3()
        {
            InitializeComponent();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            tbAge.Text = CalculateAge.GetAge(dateTimePicker1.Value).ToString();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (this.ValidateChildren())
            {
                AddDonor();
            }
        }

        private async void AddDonor()
        {
            Donor d = new Donor();
            d.bloodGroup = cbBloodGroup.Text;
            d.firstName = tbFirstName.Text;
            d.lastName = tbLastName.Text;
            d.gender = cbGender.Text;
            d.dateOfBirth = dateTimePicker1.Value;
            d.age = Convert.ToInt32(tbAge.Text);
            d.maritalStatus = cbMaritalStatus.Text;
            d.occupation = tbOccupation.Text;
            d.address1 = tbAddress1.Text;
            d.address2 = tbAddress2.Text;
            d.city = tbCity.Text;
            d.postZipCode = tbPostZipCode.Text;
            d.provState = tbProvState.Text;
            d.country = tbCountry.Text;
            d.phone = tbPhone.Text;
            d.mobile = tbMobile.Text;

            HttpResponseMessage response = await restClient.PostRequestAsync(baseUrl, d);
            tbStatus.Text = response.StatusCode.ToString();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //----------------------------------- Validation of fields -----------------------------------------------//

        private void cbBloodGroup_Validating(object sender, CancelEventArgs e)
        {
            if (cbBloodGroup.Text.Trim() == "")
            {
                errorProvider1.SetError(cbBloodGroup, "Blood Group is required");
                e.Cancel = true;
                return;
            }
            errorProvider1.SetError(cbBloodGroup, "");
        }

        private void tbFirstName_Validating(object sender, CancelEventArgs e)
        {
            if (tbFirstName.Text.Trim() == "")
            {
                errorProvider2.SetError(tbFirstName, "First Name is required");
                e.Cancel = true;
                return;
            }
            errorProvider2.SetError(tbFirstName, "");
        }

        private void tbLastName_Validating(object sender, CancelEventArgs e)
        {
            if (tbLastName.Text.Trim() == "")
            {
                errorProvider3.SetError(tbLastName, "Last Name is required");
                e.Cancel = true;
                return;
            }
            errorProvider3.SetError(tbLastName, "");
        }

        private void tbAge_Validating(object sender, CancelEventArgs e)
        {
            if (tbAge.Text.Trim() == "")
            {
                errorProvider4.SetError(tbAge, "Age is required.\r\nEnter Date of Birth");
                e.Cancel = true;
                return;
            }
            errorProvider4.SetError(tbAge, "");
        }

        private void tbAddress1_Validating(object sender, CancelEventArgs e)
        {
            if (tbAddress1.Text.Trim() == "")
            {
                errorProvider5.SetError(tbAddress1, "Address 1 is required");
                e.Cancel = true;
                return;
            }
            errorProvider5.SetError(tbAddress1, "");
        }

        private void tbCity_Validating(object sender, CancelEventArgs e)
        {
            if (tbCity.Text.Trim() == "")
            {
                errorProvider6.SetError(tbCity, "City is required");
                e.Cancel = true;
                return;
            }
            errorProvider6.SetError(tbCity, "");
        }

        private void tbPostZipCode_Validating(object sender, CancelEventArgs e)
        {
            if (tbPostZipCode.Text.Trim() == "")
            {
                errorProvider7.SetError(tbPostZipCode, "Postal or Zip Code is required");
                e.Cancel = true;
                return;
            }
            errorProvider7.SetError(tbPostZipCode, "");
        }

        private void tbProvState_Validating(object sender, CancelEventArgs e)
        {
            if (tbProvState.Text.Trim() == "")
            {
                errorProvider8.SetError(tbProvState, "Province or State is required");
                e.Cancel = true;
                return;
            }
            errorProvider8.SetError(tbProvState, "");
        }

        private void tbCountry_Validating(object sender, CancelEventArgs e)
        {
            if (tbCountry.Text.Trim() == "")
            {
                errorProvider9.SetError(tbCountry, "Country is required");
                e.Cancel = true;
                return;
            }
            errorProvider9.SetError(tbCountry, "");
        }
    }
}

