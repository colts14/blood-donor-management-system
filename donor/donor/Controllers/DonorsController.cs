﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using donor.Models;

namespace donor.Controllers
{
    [Produces("application/json", "application/xml")]
    [Route("api/[controller]")]
    public class DonorsController : Controller
    {
        private readonly IDonor _repository;

        public DonorsController(IDonor repository)
        {
            _repository = repository;
        }

        // GET: api/donors
        [HttpGet("/api/{donors=donors}.{format?}"), FormatFilter]
        public Task<IEnumerable<Donor>> GetDonorsAsync() => _repository.GetAllAsync();

        // GET: api/donors/5
        [HttpGet("/api/donors/{id}.{format?}"), FormatFilter]
        public async Task<IActionResult> GetDonor([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Donor donor = await _repository.FindAsync(id);
            if (donor == null)
            {
                return NotFound();
            }
            else
            {
                return new ObjectResult(donor);
            }
        }

        // PUT: api/donors/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDonor([FromRoute] int id, [FromBody] Donor donor)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (donor == null || id != donor.Id)
            {
                return BadRequest();
            }

            await _repository.UpdateAsync(donor);
            return new NoContentResult();  // 204
        }

        // POST: api/donors
        [HttpPost]
        public async Task<IActionResult> PostDonor([FromBody] Donor donor)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (donor == null)
            {
                return BadRequest();
            }

            await _repository.AddAsync(donor);
            // return a 201 response, Created

            return CreatedAtAction(nameof(GetDonor), new { id = donor.Id }, donor);
        }

        // DELETE: api/donors/5
        [HttpDelete("{id}")]
        public async Task DeleteDonor([FromRoute] int id)
        {
            await _repository.RemoveAsync(id);
            // void returns 204, No Content
        }

    }
}