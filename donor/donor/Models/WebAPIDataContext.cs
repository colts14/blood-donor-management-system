﻿using Microsoft.EntityFrameworkCore;

namespace donor.Models
{
    public class WebAPIDataContext : DbContext
    {
        public WebAPIDataContext(DbContextOptions<WebAPIDataContext> options) : base(options)
        { }
        public DbSet<Donor> Donors { get; set; }
    }
}
