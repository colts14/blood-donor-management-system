﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace donor.Models
{
    public class Donor
    {
        public int Id { get; set; }

        [Required]
        public String FirstName { get; set; }

        [Required]
        public String LastName { get; set; }

        public String Gender { get; set; }

        [Required]
        public DateTime DateOfBirth { get; set; }

        public int Age { get; set; }

        public String MaritalStatus { get; set; }

        public String Occupation { get; set; }

        [Required]
        public String BloodGroup { get; set; }

        [Required]
        public String Address1 { get; set; }

        public String Address2 { get; set; }

        [Required]
        public String PostZipCode { get; set; }

        [Required]
        public String City { get; set; }

        [Required]
        public String ProvState { get; set; }

        [Required]
        public String Country { get; set; }

        public String Phone { get; set; }

        public String Mobile { get; set; }

        [Required]
        public DateTime Timestamp { get; set; }
    }
}
