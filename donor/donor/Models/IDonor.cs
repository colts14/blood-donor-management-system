﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace donor.Models
{
    public interface IDonor
    {
        Task InitAsync();
        Task AddAsync(Donor d);
        Task<Donor> RemoveAsync(int id);
        Task<IEnumerable<Donor>> GetAllAsync();
        Task<Donor> FindAsync(int id);
        Task UpdateAsync(Donor d);
    }
}
