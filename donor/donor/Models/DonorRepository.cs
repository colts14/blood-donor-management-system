﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace donor.Models
{
    public class DonorRepository : IDonor, IDisposable
    {
        private WebAPIDataContext _context;

        public DonorRepository(WebAPIDataContext context)
        {
            _context = context;
        }

        public void Dispose()
        {
            _context?.Dispose();
        }

        public async Task AddAsync(Donor donor)
        {
            donor.Timestamp = DateTime.UtcNow;
            _context.Add(donor);
            await _context.SaveChangesAsync();
        }

        public Task<Donor> FindAsync(int id) =>
            _context.Donors.SingleOrDefaultAsync(m => m.Id == id);

        public async Task<IEnumerable<Donor>> GetAllAsync() =>
            await _context.Donors.ToListAsync();

        public Task InitAsync()
        {
            return Task.FromResult<object>(null);
        }

        public async Task<Donor> RemoveAsync(int id)
        {
            Donor donor = await _context.Donors.SingleOrDefaultAsync(m => m.Id == id);
            if (donor == null) return null;

            _context.Donors.Remove(donor);
            await _context.SaveChangesAsync();
            return donor;
        }

        public async Task UpdateAsync(Donor donor)
        {
            donor.Timestamp = DateTime.UtcNow;
            _context.Donors.Update(donor);
            await _context.SaveChangesAsync();
        }

        private bool DonorExists(int id)
        {
            return _context.Donors.Any(e => e.Id == id);
        }
    }
}
